## debian/tests/makecheck.tst -- GAP Test script
## script format: GAP Reference Manual section 7.9 Test Files (GAP 4r8)
##
gap> TestPackageAvailability( "float" , "=1.0.3" , true );
"/usr/share/gap/pkg/float"

##
## eos
