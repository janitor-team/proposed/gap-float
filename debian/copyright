Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Upstream-Name: float package for GAP
Upstream-Contact: Laurent Bartholdi <laurent.bartholdi@gmail.com>
Source: http://www.gap-system.org/Packages/float.html
X-Source: https://github.com/gap-packages/float
X-Source-Downloaded-From: https://github.com/gap-packages/float/releases
X-Upstream-Bugs: https://github.com/gap-packages/float/issues
Files-Excluded:
 build-aux/*~
 doc/*.css
 doc/*.js
 doc/float.toc
 doc/floatbib.xml
 doc/chap*.txt
 doc/chap*.html
 doc/chooser.html
 doc/manual.lab
 doc/manual.six
 doc/manual.pdf
 m4/ax_cxx_compile_stdcxx.m4
 m4/ax_check_compile_flag.m4
 m4/ax_compiler_vendor.m4
 m4/ax_gcc_archflag.m4
 m4/ax_gcc_x86_cpuid.m4
 m4/lt~obsolete.m4
 m4/ltsugar.m4
 m4/ltoptions.m4
 m4/ltversion.m4
 m4/libtool.m4
 build-aux/compile
 build-aux/depcomp
 build-aux/ltmain.sh
 build-aux/missing
 build-aux/config.guess
 build-aux/config.sub
 build-aux/install-sh
 src/Makefile.in
 Makefile.in
 config.h.in
 aclocal.m4
 configure

Files: *
Copyright:
 2007-2022 Laurent Bartholdi <laurent.bartholdi@gmail.com>
License: GPL-3+

Files: debian/*
Copyright:
 2014-2022 Jerome Benoit <calculus@rezozer.net>
License: GPL-3+

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in `/usr/share/common-licenses/GPL-3'.
